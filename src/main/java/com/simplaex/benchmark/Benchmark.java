package com.simplaex.benchmark;

/**
 * Created by afedulov on 12.10.16.
 */
public interface Benchmark {
    void execute(String[] args) throws Exception;
}