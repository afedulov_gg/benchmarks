package com.simplaex.benchmark;

import com.simplaex.benchmark.sql.SqlBenchmark;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;

public class App {

    public static void main(String[] args) {
        SqlBenchmark bench = new SqlBenchmark();

        try {
            bench.execute(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
