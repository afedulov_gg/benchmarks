package com.simplaex.benchmark.sql;

import com.simplaex.benchmark.Benchmark;

import java.sql.*;

/**
 * Created by afedulov on 12.10.16.
 */
public class SqlBenchmark implements Benchmark {

    @Override
    public void execute(String[] args) throws Exception {
        try
        {
            String driver = "com.mysql.jdbc.Driver";

            String url = "jdbc:mysql://rds-pepapi-dev-virginia-vpc.cpjuakzkvwua.us-east-1.rds.amazonaws.com/pep_player_profiles_dev?rewriteBatchedStatements=true&useUnicode=true&characterEncoding=UTF-8";
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(url, "multiregion", "dUZITAioXRRDivl");

            String query = "SELECT * FROM users";

            // create the java statement
            Statement st = conn.createStatement();

            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            // iterate through the java resultset
            while (rs.next())
            {
                int id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                Date dateCreated = rs.getDate("date_created");
                boolean isAdmin = rs.getBoolean("is_admin");
                int numPoints = rs.getInt("num_points");

                // print the results
                System.out.format("%s, %s, %s, %s, %s, %s\n", id, firstName, lastName, dateCreated, isAdmin, numPoints);
            }
            st.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
}
