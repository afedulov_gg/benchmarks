package com.simplaex.benchmark.elasticsearch;

import com.simplaex.benchmark.Benchmark;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.net.InetAddress;

/**
 * Created by afedulov on 12.10.16.
 */
public class ElasticSearchBenchmark implements Benchmark {

    public void execute(String[] args) throws Exception{

//        String elasticHost = "elasticdb-dev-virginia.simplaex.net";
//        String indexName = "profiles-dev";

        String elasticHost = "127.0.0.1";
        String indexName = "test";

        if(args.length > 0){
            elasticHost = args[0];
            indexName = args[1];
        }

//        Settings settings = Settings.settingsBuilder().put("cluster.name", "elastic").build();
//        final TransportClient client = TransportClient.builder().settings(settings).build();
        final TransportClient client = TransportClient.builder().build();

//        Settings settings = Settings.settingsBuilder()
//                .put("path.home", ".")
//                .put("discovery.zen.ping.unicast.hosts", "52.90.118.161").build();
//        final Node node = NodeBuilder.nodeBuilder().settings(settings).clusterName("elasticdb").client(true).node();
//        Client client = node.client();


        client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(elasticHost), 9300));


        String playerId = "4498908C-1B70-4FCD-9F3E-05843BC8FF80";
//        String id = "1076-4498908C-1B70-4FCD-9F3E-05843BC8FF80";
        String id = "5";

        String[] fields = {"gameId"};

        long startTime = System.nanoTime();
        GetResponse response = client.prepareGet(indexName, "profile", id).setFetchSource(fields, null).get();
        response.getSource().get("playerId");

        long stopTime = System.nanoTime();

        long elapsed = (stopTime - startTime) / 1000000;

        System.out.println("Elapsed ms (Get Query): " + elapsed);

        SearchRequestBuilder request = client.prepareSearch(indexName)
                .addSort("_doc", SortOrder.ASC)
                .setFetchSource(fields, null)
                .setQuery(QueryBuilders.boolQuery().filter(QueryBuilders.termQuery("playerId", playerId)));

        startTime = System.nanoTime();
        SearchResponse response1 = request.execute().get();
        stopTime = System.nanoTime();

        elapsed = (stopTime - startTime) / 1000000;

        for (SearchHit hit : response1.getHits()) {
            System.out.println(hit.getSource());
        }

        System.out.println("Elapsed ms (Term Query): " + elapsed);

    }
}
